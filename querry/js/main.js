const outputBlock = document.querySelector(".output");

function getJSON() {

}

function postJSON() {

}

function makeFunctional() {
    const getButton = document.getElementById("get-btn");
    const postButton = document.getElementById("post-btn");
    const postButton2 = document.getElementById("post-btn2");
    const postForm = document.querySelector(".post_block__form-block");
    const outputBlock = document.querySelector(".output");
    const queryModel = new queries();
    
    getButton.addEventListener('click', async (e) => {
        let mass = await queryModel.getInfo('http://localhost/denwer/querry/01data.json', 'GET');
        outputMass(mass);
        console.log("UE")
    });

    postButton.addEventListener('click', async (e) => {
        getButton.classList.add("hidden");
        postButton.classList.add("hidden");
        outputBlock.classList.add('hidden');
        postForm.classList.remove('hidden');
    })

    postButton2.addEventListener('click', async (e) => {
        let mass = await queryModel.getInfo('http://localhost/denwer/querry/01data.json', 'POST');
        let addObj = {
            firstName: document.getElementById("firstname").value,
            lastName: document.getElementById("lastname").value,
            age: document.getElementById("age").value,
        }
        mass.push(addObj);
        outputMass(mass);
        getButton.classList.remove("hidden");
        postButton.classList.remove("hidden");
        outputBlock.classList.remove('hidden');
        postForm.classList.add('hidden');
    })

    function outputMass(mass) {
        let result = "[ ";
        for (let i of mass) {
            result += "{";
            result += i.firstName;
            result += ', ';
            result += i.lastName;
            result += ', ';
            result += i.age;
            result += "} ";
        }
        result += "]";
        outputBlock.innerText = result;
    }
}

function queries() {

}

queries.prototype.getInfo = async function(url, method) {
    let response = await fetch(url, {
        method: method
    })
    .then(response => {
        if (!response.ok) {
            throw new Error("Failed with HTTP code " + response.status);
        }
        return response
    })
    .then(result  => result.json())
    .then(data => {
        return data
    });
    return response;
}

makeFunctional();